package ex2;

import jdk.jshell.spi.ExecutionControl;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Shop {
    private List<Product> products;

    public void update(Product product) {
        if (product.getQuality() <= 0) {
            product.setQuality(0);
            return;
        }

        product.setSellIn(product.getSellIn()-1);
        int qualityUpdater = 1;
        int qualityWeight = 1;

        if(product.getName().equals("brie vieilli"))qualityUpdater = -qualityUpdater;
        if(product.getType().equals("laitier"))qualityWeight = qualityWeight*2;
        if(product.getSellIn() <= 0)qualityWeight = qualityWeight*2;

        product.setQuality(product.getQuality()-(qualityUpdater*qualityWeight));

        if(product.getQuality() >= 50) {
            product.setQuality(50);
        }
    }
}
