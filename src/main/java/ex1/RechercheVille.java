package ex1;

import java.util.List;
import java.util.stream.Collectors;

public class RechercheVille {
    private List<String> villes;

    public RechercheVille(List<String> villes){
        this.villes = villes;
    }

    public List<String> rechercher(String mot) throws NotFoundException{
        if(mot.equals("*")) return villes;
        if(mot.length() >= 2) return villes.stream().filter(x -> x.toLowerCase().contains(mot.toLowerCase())).collect(Collectors.toList());
        throw new NotFoundException();
    }
}
