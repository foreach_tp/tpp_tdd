package ex2;

import jdk.jshell.spi.ExecutionControl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ShopTest {

    static Shop shop;
    static List<Product> products;

    @Before
    public void setup(){
        products = Arrays.asList(
                new Product("fruit", "pomme", 3, 10),
                new Product("fromage", "brie vieilli", 10, 5),
                new Product("laitier", "lait", 3, 6),
                new Product("fruit", "poire",1,2),
                new Product("patisserie","pain au chocolat",4,52)
        );

        shop = new Shop(products);
    }

    @Test
    public void whenUpdateProduct_thenProductSellInDecrements() throws ExecutionControl.NotImplementedException {
        Product pommeBeforeTest = new Product("fruit", "pomme", 3, 10);

        shop.update(shop.getProducts().get(0));
        Product result = shop.getProducts().get(0);

        Assert.assertTrue(result.getSellIn() < pommeBeforeTest.getSellIn());
    }

    @Test
    public void whenUpdateProduct_thenProductQualityDecrements() throws ExecutionControl.NotImplementedException {
        Product pommeBeforeTest = new Product("fruit", "pomme", 3, 10);

        shop.update(shop.getProducts().get(0));
        Product result = shop.getProducts().get(0);

        Assert.assertTrue(result.getQuality() < pommeBeforeTest.getQuality());
    }

    @Test
    public void whenProductSellInLesserThan0_thenQualityDecrementsBy2(){
        Product poireBeforeTest = new Product("fruit", "poire",1,2);

        shop.update(shop.getProducts().get(3));
        Product result = shop.getProducts().get(3);

        Assert.assertEquals(poireBeforeTest.getQuality()-2,result.getQuality());
    }

    @Test
    public void whenProductQuality0_thenDontDecrementsFurther(){

        shop.update(shop.getProducts().get(3));
        shop.update(shop.getProducts().get(3));
        Product result = shop.getProducts().get(3);

        Assert.assertEquals(0,result.getQuality());
    }

    @Test
    public void whenProductQualityGreater50_thenQuality50(){

        shop.update(shop.getProducts().get(4));
        Product result = shop.getProducts().get(4);

        Assert.assertTrue(50 >= result.getQuality());
    }

    @Test
    public void whenProductNameBrieVeilli_uthenQualityIncrements(){
        Product brie = new Product("fromage", "brie vieilli", 10, 5);

        shop.update(shop.getProducts().get(1));
        Product result = shop.getProducts().get(1);

        Assert.assertTrue(brie.getQuality() < result.getQuality());
    }

    @Test
    public void whenProductTypeLaitier_thenQualityDecrementsFasterBy2(){
        Product lait = new Product("laitier", "lait", 3, 6);

        shop.update(shop.getProducts().get(2));
        Product result = shop.getProducts().get(2);

        Assert.assertEquals(lait.getQuality()-2, result.getQuality());
    }



}
