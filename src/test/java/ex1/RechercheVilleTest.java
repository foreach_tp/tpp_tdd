package ex1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class RechercheVilleTest {

    static RechercheVille rechercheVille;
    static List<String> villes;

    @Before
    public void setup(){
        villes = Arrays.asList("Budapest", "Skopje", "Rotterdam", "Valence", "Vancouver", "Amsterdam", "Vienne", "Sydney",
                "New York", "Londres", "Bangkok", "Hong Kong", "Dubaï", "Rome", "Istanbul");
        rechercheVille = new RechercheVille(villes);
    }

    @Test
    public void whenMotSizeLesserThan2_thenThrowsNotFoundException() {
        String motRechercher = "t";
        Assert.assertThrows(NotFoundException.class,() -> rechercheVille.rechercher(motRechercher));
    }

    @Test
    public void whenMotSizeGreaterOrEquals2_thenVilleStartingWithMot() throws NotFoundException {
        String motRechercher = "Va";
        List<String> expected = Arrays.asList("Valence","Vancouver");

        List<String> result = rechercheVille.rechercher(motRechercher);

        Assert.assertEquals(expected,result);
    }

    @Test
    public void whenMotAllCapsSizeGreaterOrEquals2_thenVilleStartingWithMot() throws NotFoundException {
        String motRechercher = "VA";
        List<String> expected = Arrays.asList("Valence","Vancouver");

        List<String> result = rechercheVille.rechercher(motRechercher);

        Assert.assertEquals(expected,result);
    }

    @Test
    public void whenMotAllMinSizeGreaterOrEquals2_thenVilleStartingWithMot() throws NotFoundException {
        String motRechercher = "va";
        List<String> expected = Arrays.asList("Valence","Vancouver");

        List<String> result = rechercheVille.rechercher(motRechercher);

        Assert.assertEquals(expected,result);
    }

    @Test
    public void whenMotMatchesPartsOfCity_thenVileCaontainingMot() throws NotFoundException{
        String motRechercher = "ape";
        List<String> expected = Arrays.asList("Budapest");

        List<String> result = rechercheVille.rechercher(motRechercher);

        Assert.assertEquals(expected,result);
    }

    @Test
    public void whenWildCard_thenAllVilles() throws NotFoundException{
        String motRechercher = "*";
        List<String> expected = villes;

        List<String> result = rechercheVille.rechercher(motRechercher);

        Assert.assertEquals(expected,result);
    }
}
